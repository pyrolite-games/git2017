﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EventManager : MonoBehaviour {

	public delegate void ClickAction ();
	public static event ClickAction OnLeftClick;
	public static event ClickAction OnRightClick;


	void Update () {
		if (Input.GetMouseButtonDown (0)) {
			if (OnLeftClick != null) {
				OnLeftClick ();
			}
		}
		if (Input.GetMouseButtonDown (1)) {
			if (OnRightClick != null) {
				OnRightClick ();
			}
		}
	}
	
}
