﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChangeColor : MonoBehaviour {


	void OnEnable () {
		EventManager.OnLeftClick += SetColor;
	}


	void OnDisable () {
		EventManager.OnLeftClick -= SetColor;
	}


	public void SetColor () {
		Color col = new Color (Random.value, Random.value, Random.value);
		GetComponent<Renderer> ().material.color = col;
	}

}
